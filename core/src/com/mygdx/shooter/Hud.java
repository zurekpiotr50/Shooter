package com.mygdx.shooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Hud {
    SpriteBatch batch;
    HpBar hpBar;

    OrthographicCamera hudcamera;

    Hud(Player player)
    {
        hudcamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        hudcamera.update();

        batch = new SpriteBatch();
        batch.getProjectionMatrix().set(hudcamera.combined);

        hpBar = new HpBar(player, Gdx.graphics.getWidth() / 2 ,
                Gdx.graphics.getHeight() / 15 , true ,
                - Gdx.graphics.getWidth() / 4 ,
                Gdx.graphics.getHeight() * 6.5f / 15);
    }

    void render(){
        batch.begin();
        hpBar.draw(batch);
        batch.end();

    }
    void finish(){
        batch.dispose();
    }

}
