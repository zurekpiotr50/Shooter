package com.mygdx.shooter;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class HpBar {

    static Texture bg , gb;

    Sprite backgroung;
    Sprite greenbar;

    Player player;
    float sizeX , sizeY;
    boolean flag ; // czy rysować gdy hp == maxhp

//    public static void loaddata(Assets assets)
    static {
        bg = new Texture( "hpbar.png" );
        gb = new Texture( "greenHpBar.png" );
    }

    public void init()
    {

    }

    public HpBar(Player player, float sizeX , float sizeY , boolean draw_if_full_hp , float x , float y)
    {
        this.player = player;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.flag = draw_if_full_hp;
        backgroung = new Sprite(bg);
        greenbar = new Sprite(gb);

        backgroung.setSize(sizeX, sizeY);
        greenbar.setSize(sizeX, sizeY);
        backgroung.setPosition(x, y);
        greenbar.setPosition(x, y);
    }



    public void draw(SpriteBatch batch, float x , float y)
    {
        greenbar.setSize(sizeX* player.hp / player.maxHp, sizeY);
        backgroung.setPosition(x, y);
        greenbar.setPosition(x, y);
        if(flag)
        {
            backgroung.draw(batch);
            greenbar.draw(batch);
        }
        else if(player.hp >= player.maxHp)
        {
            backgroung.draw(batch);
            greenbar.draw(batch);
        }

    }
    public void draw(SpriteBatch batch)
    {
        greenbar.setSize(sizeX*player.hp/player.maxHp, sizeY);
        //backgroung.setPosition(x, y);
        //greenbar.setPosition(x, y);
        //System.err.println("test");
        if(flag)
        {
            backgroung.draw(batch);
            greenbar.draw(batch);
        }
        else if(player.hp >= player.maxHp)
        {
            backgroung.draw(batch);
            greenbar.draw(batch);
        }
    }

}