package com.mygdx.shooter;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Disposable;

public class Assets implements Disposable {
    public final AssetManager manager = new AssetManager();

    public void loadData()
    {

    }

    @Override
    public void dispose()
    {
        manager.dispose();
    }
}
