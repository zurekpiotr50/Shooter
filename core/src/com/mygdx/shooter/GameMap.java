package com.mygdx.shooter;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import java.lang.reflect.Array;
import java.util.*;

public class GameMap {

    Map<Vector2, Tile> worldMap;
    World world;

    static int tileWidth = 50, tileHeight = 50;
    int[][] template = 
            {{1,1,0,1,1},
            {1,0,0,0,1},
            {0,0,0,0,0},
            {1,0,0,0,1},
            {1,1,0,1,1}};
    GameMap(World world)
    {
        this.world = world;
        worldMap = new HashMap<>();

        for(int i = -2 ; i < 3 ; i ++)
        {
            for(int j = -2 ; j < 3 ; j++)
            {
                Vector2 pos = new Vector2(i * tileHeight, j * tileWidth);
                if(template[i+2][j+2] == 1)
                {

                    worldMap.put(pos, new Wall(world, pos));
                }
                else
                {
                    worldMap.put(pos , new Ground(pos));
                }
            }
        }
    }




    public List<Vector2> getPath(Vector2 from, Vector2 to)
    {
        if( getTile(from) == null || getTile(to) == null)
            return null;

        List<Vector2> result = new ArrayList<>();
        result.add(from);

        LinkedList<Tile> path = BFS(from, to);
        if(path != null)
        {
//            path.poll();
            for(Tile tile :path)
            {
                if(tile != getTile(from) && tile != getTile(to))
                    result.add(tile.pos);
            }
        }

        result.add(to);

        return result;
    }

    private LinkedList<Tile> BFS(Vector2 start, final Vector2 dest)
    {
        Comparator<LinkedList<Tile>> comp = new Comparator<LinkedList<Tile>>() {
            @Override
            public int compare(LinkedList<Tile> o1, LinkedList<Tile> o2) {
                if( o1.peekLast().getPos().dst(dest) > o2.peekLast().getPos().dst(dest) )
                    return 1;
                if( o1.peekLast().getPos().dst(dest) < o2.peekLast().getPos().dst(dest) )
                    return -1;
                return 0;
            }
        };
        PriorityQueue<LinkedList<Tile>> queue = new PriorityQueue<LinkedList<Tile>>(10,comp);

        Map<Tile, Boolean> visited = new HashMap<>();
        ArrayList<Vector2> directions = new ArrayList<>();

        directions.add(new Vector2(0,tileWidth));
        directions.add(new Vector2(tileHeight, 0));
        directions.add(new Vector2(0,-tileWidth));
        directions.add(new Vector2(-tileHeight, 0));

        queue.add( new LinkedList<Tile>() );
        queue.peek().add( getTile(start) );


        while(!queue.isEmpty())
        {
            LinkedList<Tile> path = queue.poll();
            Tile c = path.peekLast();
            visited.put(c, true);

            if( c == getTile(dest))
            {
                return path;
            }
            for(Vector2 dir: directions)
            {
                Vector2 neightbour = c.getPos().cpy().sub(dir);
                Tile neightbourTile = getTile( neightbour );
                if(neightbourTile != null && !(neightbourTile instanceof Wall) ){
                    if(visited.containsKey( getTile(neightbour) ))
                        continue;

                    LinkedList<Tile> newPath = (LinkedList<Tile>) path.clone();
                    newPath.add(getTile(neightbour));
                    queue.add( newPath );
                }
            }

        }
        return null;
    }

    public Tile getTile(Vector2 playerPos) {
        return getTile(playerPos.x , playerPos.y);
    }

    public Tile getTile(int x, int y)
    {
        Vector2 pos = new Vector2(x * tileHeight, y * tileWidth);

        if(worldMap.containsKey(pos))
        {
            return worldMap.get(pos);
        }
        return null;
    }

    public Tile getTile(float x, float y)
    {
        int ix = ((int) Math.floor((x+25)/50f));
        int iy = ((int) Math.floor((y+25)/50f));
        return getTile(ix, iy);
    }

    public void draw(Batch batch, Vector2 playerPosition)
    {
        int x =((int) Math.floor((playerPosition.x+25)/50f));
        int y =((int) Math.floor((playerPosition.y+25)/50f));

        for(int i = -7 ; i < 8 ; i ++)
        {
            for(int j = -7 ; j < 8 ; j++)
            {
//                if(i*i + j*j > 9)
//                    continue;
                Vector2 pos = new Vector2((i+x) * tileHeight, (j+y) * tileWidth);
                if(pos.dst(playerPosition) > 8f*50)
                    continue;
                if(!worldMap.containsKey(pos))
                {
                    if(new Random().nextInt(4) < 1 )
                        worldMap.put(pos, new Wall(world, pos));
                    else
                        worldMap.put(pos , new Ground(pos));
                }
                Tile tile = worldMap.get(pos);
                tile.draw(batch);

            }
        }

    }


}
abstract class Tile
{
    protected Sprite sprite;
    Vector2 pos;
    Tile(Vector2 pos)
    {
        this.pos = pos;
        sprite = new Sprite();
//        sprite.setPosition(pos.x,pos.y);
        sprite.setSize(GameMap.tileWidth, GameMap.tileHeight);
        sprite.setCenter(pos.x,pos.y);

    }

    public Vector2 getPos() {
        return pos;
    }

    public void draw(Batch batch)
    {
        if(sprite != null)
            sprite.draw(batch);
//            batch.draw(sprite);
    }

    void setSprite(TextureRegion textureRegion)
    {
        sprite.setRegion(textureRegion);
//        ,
//                textureRegion.getRegionX(),
//                textureRegion.getRegionY(),
//                textureRegion.getRegionWidth(),
//                textureRegion.getRegionHeight());

    }
    void setSprite(Texture texture)
    {
        sprite.setTexture(texture);
    }
}

class Ground extends Tile
{
    static TextureRegion[][] textureRegions;
    static
    {
        Texture grassTexture = new Texture("TX Tileset Grass.png");
        textureRegions = TextureRegion.split(grassTexture,
                grassTexture.getWidth() / 8,
                grassTexture.getHeight() / 8);
    }
    Ground(Vector2 pos)
    {
        super(pos);

        setSprite(textureRegions[3 + new Random().nextInt(5)][new Random().nextInt(2)]);

    }

}
class Wall extends Tile {

    protected BodyDef bodyDef;
    protected Body body;
    protected Fixture fixture;

    static FixtureDef fixtureDef = new FixtureDef();
    static {

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(25,25);
        fixtureDef.shape = polygonShape;
        fixtureDef.density = 1f;
        fixtureDef.friction = 1f;
        fixtureDef.restitution = 0f;

    }

    static TextureRegion textureRegions;
    static Texture wallTexture;
    static
    {
        wallTexture = new Texture("TX Tileset Wall.png");

        textureRegions = new TextureRegion(wallTexture,32,288,64,64);
    }

    Wall(World world, Vector2 pos)
    {
        super(pos);
        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
//        bodyDef.position.set(0,0);

        body = world.createBody(bodyDef);
        fixture = body.createFixture(fixtureDef);
        body.setTransform(pos, 0);
        body.setUserData(this);


        setSprite(textureRegions);
    }

}