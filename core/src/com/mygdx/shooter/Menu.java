package com.mygdx.shooter;

import javax.swing.JList.DropLocation;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.sun.org.apache.bcel.internal.generic.ISTORE;



public class Menu implements Screen{

    final Engine engine;

    Texture playButton, sterowanie;
    SpriteBatch batch;

    public Menu(Engine engine)
    {
        this.engine = engine;
        playButton = new Texture("play.png");
        sterowanie = new Texture( "sterowanie.png");
        batch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {

        batch.begin();
        batch.draw(playButton , Gdx.graphics.getWidth()/2-100 , 0 , 200 , 100 );
        batch.draw(sterowanie, 0,Gdx.graphics.getHeight()-sterowanie.getHeight());
        batch.end();
        if(Gdx.input.isTouched())
        {
            engine.shooterGame = new ShooterGame(engine);
            engine.setScreen(engine.shooterGame);
        }
    }

    @Override
    public void dispose() {
        batch.dispose();
        playButton.dispose();
    }

    @Override
    public void resize(int width, int height) {

    }




    @Override
    public void show() {
        // TODO Auto-generated method stub

    }



    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }


}