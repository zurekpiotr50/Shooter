package com.mygdx.shooter;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import java.util.List;

public class Zombie extends DynamicObject{
    static FixtureDef fixtureDef = new FixtureDef();
    static {
        CircleShape circle = new CircleShape();
        circle.setRadius(12f);
        fixtureDef.shape = circle;
        fixtureDef.density = 0.5f;
        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.6f;
    }
    private Animation[] moveAnimation = new Animation[4];
    private Animation[] standAnimation = new Animation[4];

    List<Vector2> pathToPlayer;

    float attackSpeed = 0.5f;
    float cooldown = 0f;

    Texture green, red;
    int maxHp = 100;
    int hp = 100;

    Zombie(GameWorld gameWorld, Vector2 position)
    {
        super(gameWorld, fixtureDef);
        body.setTransform(position, 0);

        texture = new Texture("zombie7.png");
        red = new Texture("red.png");
        green = new Texture("green.png");

        animationFrames = TextureRegion.split(texture,
                texture.getWidth() / 3,
                texture.getHeight() / 4);

        moveAnimation[0] = new Animation<> (1f/8f, animationFrames[2]);
        moveAnimation[1] = new Animation<> (1f/8f, animationFrames[3]);
        moveAnimation[2] = new Animation<> (1f/8f, animationFrames[1]);
        moveAnimation[3] = new Animation<> (1f/8f, animationFrames[0]);
        standAnimation[0] = new Animation<>( 1f/8f, animationFrames[2][0]);
        animation = moveAnimation[3];


    }
    Zombie(GameWorld gameWorld) {
        this(gameWorld, new Vector2(150,0));
    }

//    @Override
    public void step(float dTime) {
        super.step(dTime);

        if(hp <= 0)
        {
            isDead = true;
            return;
        }
        if(gameWorld.player.getPosition().sub(getPosition()).len() > 400)
        {
            isDead = true;
            return;
        }

        Vector2 playerPos = gameWorld.player.getPosition();
        gameWorld.gameMap.getTile(playerPos);

        pathToPlayer = gameWorld.gameMap.getPath(
                body.getPosition(),
                gameWorld.player.getPosition());

        if(pathToPlayer != null)
        {
            body.setLinearVelocity(
                    body.getPosition().cpy().sub(pathToPlayer.get(1)).scl(-1).setLength(80) );
        }

        cooldown += dTime;

    }

    @Override
    public void solveContact(Object object)
    {
        if(object instanceof Player)
        {
            Player player = (Player) object;

            if(cooldown > 0)
            {
                player.hp -= 10;
                cooldown = -1 / attackSpeed;
            }
        }
    }


//    @Override
    public void draw(Batch batch)
    {
        TextureRegion frame = animation.getKeyFrame(animationTime,true);
        batch.draw(frame ,
                getPosition().x - frame.getRegionWidth()/2f,
                getPosition().y - frame.getRegionHeight()/6f);

        if(hp < maxHp)
        {
            batch.draw(red,
                    getPosition().x - frame.getRegionWidth()/2f ,
                    getPosition().y + frame.getRegionHeight()/1.5f ,
                    frame.getRegionWidth(),
                    4);

            batch.draw(green,
                    getPosition().x - frame.getRegionWidth()/2f ,
                    getPosition().y + frame.getRegionHeight()/1.5f ,
                    frame.getRegionWidth()*((float)hp/(float) maxHp),
                    4);
        }

    }
}
