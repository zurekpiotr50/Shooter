package com.mygdx.shooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import java.awt.*;

public abstract class DynamicObject extends BasicObject {

    private float MAX_SPEED = 20f;
    public boolean isDead = false;

    DynamicObject(GameWorld gameWorld, FixtureDef fixtureDef)
    {
        super(gameWorld,fixtureDef);
    }


    public void applyLinearImpulse(float impulseX, float impulseY)
    {
        body.applyLinearImpulse(new Vector2(impulseX,impulseY), body.getWorldCenter() , true);
    }

    public void applyFriction()
    {
        if(body.getLinearVelocity().len() < 6)
        {
            body.setLinearVelocity(new Vector2(0,0));
        }
        else
        {
            body.setLinearVelocity( body.getLinearVelocity().setLength( body.getLinearVelocity().len()-6)  );
        }


    }

    public void solveContact(Object object)
    {
    }



}
