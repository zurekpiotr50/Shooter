package com.mygdx.shooter;

import box2dLight.RayHandler;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.ScreenUtils;


public class ShooterGame implements Screen {

	public static boolean DEBUG = false;
	SpriteBatch batch;
	Box2DDebugRenderer debugRenderer;

	OrthographicCamera camera;

	World box2DWorld;
	RayHandler rayHandler;
	GameWorld gameWorld;
	Hud hud;

	final Engine engine;
//	Assets assets;

	float ZOOM = 1f;


	ShooterGame(Engine engine) {
		Box2D.init();
		this.engine = engine;
		debugRenderer = new Box2DDebugRenderer();
		camera = new OrthographicCamera(Gdx.graphics.getWidth()/ZOOM, Gdx.graphics.getHeight()/ZOOM);
		batch = new SpriteBatch();

		box2DWorld = new World( new Vector2(0, 0), true);
		rayHandler = new RayHandler(box2DWorld);
		gameWorld = new GameWorld(box2DWorld, rayHandler);

		gameWorld.player.coneLight.setColor(0,0,0,0);
		gameWorld.player.pointLight.setColor(new Color(0.2f,0.1f,0,1f));

		hud = new Hud(gameWorld.player);

	}

	@Override
	public void render (float v) {
		mainLoop();

		camera.update();
		batch.setProjectionMatrix(camera.combined);
		
		camera.position.x += (gameWorld.player.body.getPosition().x - camera.position.x)/4;
		camera.position.y += (gameWorld.player.body.getPosition().y - camera.position.y)/4;

		ScreenUtils.clear(1, 1, 1, 0);

		rayHandler.update();
		rayHandler.setCombinedMatrix(camera);

		batch.begin();

		gameWorld.draw(batch);

		batch.end();

		rayHandler.render();

		hud.render();

		if(DEBUG)
		{
			debugRenderer();
		}

	}

	private void mainLoop()
	{
		boolean move = false;

		if (Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) )
		{
			if (Gdx.input.isKeyPressed(Input.Keys.A)) {
				move = true;
				gameWorld.player.applyLinearImpulse(-100.80f, 0);
			}
			if (Gdx.input.isKeyPressed(Input.Keys.D)) {
				move = true;
				gameWorld.player.applyLinearImpulse(100.80f, 0);
			}
			if (Gdx.input.isKeyPressed(Input.Keys.W)) {
				move = true;
				gameWorld.player.applyLinearImpulse(0, 100.80f);
			}
			if (Gdx.input.isKeyPressed(Input.Keys.S)) {
				move = true;
				gameWorld.player.applyLinearImpulse(0, -100.80f);
			}

			Vector2 tmp = new Vector2();
			if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
				tmp = tmp.add(new Vector2(0,10));
//				gameWorld.player.shot(new Vector2(0,100));
			}
			if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
				tmp = tmp.add(new Vector2(10,0));
//				gameWorld.player.shot(new Vector2(100,0));
			}
			if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
				tmp = tmp.add(new Vector2(0,-10));
//				gameWorld.player.shot(new Vector2(0,-100));
			}
			if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
				tmp = tmp.add(new Vector2(-10,0));
//				gameWorld.player.shot(new Vector2(-100,0));
			}
			if(tmp.len() > 1)
			{
				tmp = tmp.setLength(200);
				gameWorld.player.shot(tmp);
			}


			if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
				gameWorld.player.pointLight.setColor(0,0,0,0);
				gameWorld.player.coneLight.setColor(new Color(0.2f,0.1f,0,1f));
			}
			if (Gdx.input.isKeyPressed(Input.Keys.E)) {
				gameWorld.player.coneLight.setColor(0,0,0,0);
				gameWorld.player.pointLight.setColor(new Color(0.2f,0.1f,0,1f));
			}

			if ( Gdx.input.isKeyPressed(Input.Keys.P) )
			{
				DEBUG = !DEBUG;
			}

		}
		if( !move )
		{
			gameWorld.player.applyFriction();
		}

		gameWorld.step(1f/60f);

		if( gameWorld.player.hp <= 0)
		{
			engine.setScreen(engine.menu);
		}

	}


	private void debugRenderer()
	{


//		float meshSize = 1000;
//		for(float i = 0 ; i < meshSize ; i +=100f)
//		{
//			ShapeRenderer shapeRenderer = new ShapeRenderer();
//			shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
//			shapeRenderer.setColor(Color.RED);
//			shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
//			shapeRenderer.line(new Vector2(i,0),new Vector2(i,meshSize));
//			shapeRenderer.end();
//
//			shapeRenderer.setColor(Color.GREEN);
//			shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
//			shapeRenderer.line(new Vector2(0,i),new Vector2(meshSize,i));
//			shapeRenderer.end();
//		}

//		List<Vector2> path = gameWorld.gameMap.getPath(
//				gameWorld.zombie.getPosition(),
//				gameWorld.player.getPosition());

//		if( path != null)
//		for(int i = 0 ; i < path.size()-1 ; i ++)
//		{
//			ShapeRenderer shapeRenderer = new ShapeRenderer();
//			shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
//			shapeRenderer.setColor(Color.BLUE);
//			shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
//			shapeRenderer.line(path.get(i),path.get(i+1));
//			shapeRenderer.end();
//		}

		debugRenderer.render(box2DWorld, camera.combined);
	}

	@Override
	public void show() {

	}


	@Override
	public void resize(int i, int i1) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose () {
		batch.dispose();
	}
}
