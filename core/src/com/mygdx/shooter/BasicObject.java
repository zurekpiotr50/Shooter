package com.mygdx.shooter;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public abstract class BasicObject {

    protected Texture texture;
    protected TextureRegion[][] animationFrames;
    protected Animation<TextureRegion> animation;
    protected BodyDef bodyDef;
    protected Body body;
    protected Fixture fixture;

    protected float lifeTime = 0;
    protected float animationTime = 0;

    protected GameWorld gameWorld;
    BasicObject(GameWorld gameWorld , FixtureDef fixtureDef)
    {
        this.gameWorld = gameWorld;
        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
//        bodyDef.position.set(0,0);

        body = gameWorld.world.createBody(bodyDef);
        fixture = body.createFixture(fixtureDef);
        body.setUserData(this);
    }

    public Vector2 getPosition()
    {
        return body.getPosition();
    }

    public void step(float dTime)
    {
        lifeTime += dTime;
        animationTime += dTime;
    }

    public void draw(Batch batch)
    {
        TextureRegion frame = animation.getKeyFrame(animationTime,true);
        batch.draw(frame ,
                getPosition().x - frame.getRegionWidth()/2f,
                getPosition().y- frame.getRegionHeight()/2f);
    }

    public void selfDestroy()
    {
        gameWorld.world.destroyBody(body);
    }



}
