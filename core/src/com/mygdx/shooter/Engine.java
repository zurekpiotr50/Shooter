package com.mygdx.shooter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class Engine  extends Game{

    Menu menu;
    ShooterGame shooterGame;

    @Override
    public void create() {
        menu = new Menu(this);

        setScreen(menu);
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (screen != null) screen.render(Gdx.graphics.getDeltaTime());
    }

    @Override
    public void resize(int width, int height) {
        if (screen != null) screen.resize(width, height);
    }


    @Override
    public void dispose() {
        if (screen != null) screen.dispose();
    }

    @Override
    public void setScreen(Screen screen) {
        screen.resume();
        super.setScreen(screen);
    }

    @Override
    public void pause () {
        if (screen != null) screen.pause();
    }


}
