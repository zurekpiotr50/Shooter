package com.mygdx.shooter;

import box2dLight.RayHandler;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;

import javax.swing.*;
import java.awt.*;
import java.util.*;

import static java.lang.System.exit;

public class GameWorld {

    GameMap gameMap;
    World world;
    RayHandler rayHandler;
    Player player;
    Zombie zombie;
    Array<BasicObject> listOfObject;
    Array<Zombie> listOfZombie;

    Collection<Contact> contactArray;

    float gameTime = 0;

    GameWorld(World world, RayHandler rayHandler)
    {
        this.world = world;

        this.rayHandler = rayHandler;
        rayHandler.setBlurNum(3);
        if(ShooterGame.DEBUG)
            rayHandler.setAmbientLight(new Color(0.0f, 0.0f, 0.0f, 0.12f));
        rayHandler.setShadows(true);

        gameMap = new GameMap(world);

        player = new Player(this);

        zombie = new Zombie(this);

        listOfObject = new Array<>();
        listOfZombie = new Array<>();
        listOfObject.add(player);
        listOfObject.add(zombie);
        listOfZombie.add(zombie);

        contactArray = Collections.synchronizedCollection(new ArrayList<Contact>());

        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
//                if(contact.getFixtureA().getBody().getUserData() instanceof DynamicObject
//                        && contact.getFixtureB().getBody().getUserData() instanceof DynamicObject)
                {
                    contactArray.add(contact);
                    solveSingleContact(contact);
                }
            }

            @Override
            public void endContact(Contact contact) {
//                if(contact.getFixtureA().getBody().getUserData() instanceof DynamicObject
//                        && contact.getFixtureB().getBody().getUserData() instanceof DynamicObject)
                {
                    if(contactArray.contains(contact))
                    {
                        contactArray.remove(contact);
                    }
                }
            }

            @Override
            public void preSolve(Contact contact, Manifold manifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse contactImpulse) {

            }
        });


    }

    public void addObjectToSimulation(BasicObject obj)
    {
        listOfObject.add(obj);
        if( obj instanceof Zombie )
        {

            listOfZombie.add((Zombie)obj);
        }
    }

    public void removeObjectFromSimulation(BasicObject obj)
    {
        if( obj instanceof Zombie )
        {
            listOfZombie.removeValue((Zombie)obj,true);
        }

        listOfObject.removeValue(obj, true);
        obj.selfDestroy();
    }

    public void draw(Batch batch) {

        gameMap.draw(batch,player.getPosition());
        for (BasicObject object : listOfObject ) {
            object.draw(batch);
        }
//        zombie.draw(batch);

    }


    public void step(float dTime) {

        gameTime += dTime;
        world.step(dTime, 6, 2);

        for (BasicObject object : listOfObject ) {
            object.step(dTime);
        }

        for (Contact contact : contactArray)
        {
            solveSingleContact(contact);
        }

        for(BasicObject object : listOfObject )
        {
            if( object instanceof DynamicObject )
            {
                if( ((DynamicObject) object).isDead )
                {
                    removeObjectFromSimulation(object);
                }
            }
        }

        if( listOfZombie.size < 2 + gameTime/10f )
        {
            Random random = new Random();
            Vector2 tmp = new Vector2(300f + 200f*random.nextFloat(),0);
            tmp = tmp.rotateDeg(360f*random.nextFloat());

            Zombie zombie = new Zombie(this, player.getPosition().add(tmp) );
            addObjectToSimulation(zombie);
        }

    }

    private void solveSingleContact(Contact contact)
    {
        try
        {
            DynamicObject a = null, b= null;
            if( contact.getFixtureA().getBody().getUserData() instanceof DynamicObject )
                a = (DynamicObject) contact.getFixtureA().getBody().getUserData();

            if( contact.getFixtureB().getBody().getUserData() instanceof DynamicObject )
                b = (DynamicObject) contact.getFixtureB().getBody().getUserData();

            if( a != null )
            {
                a.solveContact(contact.getFixtureB().getBody().getUserData());
            }
            if( b!= null )
            {
                b.solveContact(contact.getFixtureA().getBody().getUserData());
            }
        }
        catch (Exception e)
        {

        }
    }
}
