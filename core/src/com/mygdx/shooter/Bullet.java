package com.mygdx.shooter;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class Bullet extends DynamicObject{

    static FixtureDef fixtureDef = new FixtureDef();
    static {
        CircleShape circle = new CircleShape();
        circle.setRadius(3f);
        fixtureDef.shape = circle;
        fixtureDef.density = 0.00001f;
        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.6f;
    }


    Bullet(GameWorld gameWorld, Vector2 position, Vector2 direction) {
        super(gameWorld, fixtureDef);

        Vector2 tmp = direction.limit(16);
        position = position.add(tmp);

        body.setTransform(position, 0);

        texture = new Texture("bullet.png");
        animation = new Animation<>(1f,
                TextureRegion.split(texture,
                texture.getWidth()/3,
                texture.getHeight())[0]);

        applyLinearImpulse(direction.x, direction.y);
    }

    public void step(float dTime) {
        super.step(dTime);
        if(lifeTime > 2)
        {
            isDead = true;
        }
    }

    @Override
    public void solveContact(Object object)
    {
        if(object instanceof Zombie)
        {
            Zombie zombie = (Zombie) object;

            zombie.hp -= 10;
            isDead = true;

        }
//        if( object instanceof Wall )
        else
        {
            isDead = true;
        }
    }

    public void draw(Batch batch)
    {
        TextureRegion frame = animation.getKeyFrame(animationTime,true);
        batch.draw(frame ,
                getPosition().x - frame.getRegionWidth()/2f,
                getPosition().y );
    }
}
