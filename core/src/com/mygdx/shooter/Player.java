package com.mygdx.shooter;

import box2dLight.ConeLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public class Player extends DynamicObject{
    static FixtureDef fixtureDef = new FixtureDef();
    static {
        CircleShape circle = new CircleShape();
        circle.setRadius(10f);
//        PolygonShape shape = new PolygonShape(circle);
        fixtureDef.shape = circle;
        fixtureDef.density = 0.1f;
        fixtureDef.friction = 0.0f;
        fixtureDef.restitution = 0.0f;
    }
    public PointLight pointLight;
    public ConeLight coneLight;
    private Animation[] moveAnimation = new Animation[4];
    private Animation[] standAnimation = new Animation[4];

    int hp, maxHp;

    float shootCooldown = 0;
    float attackSpeed = 8;

    Player(GameWorld gameWorld) {
        super(gameWorld,fixtureDef);
        texture = new Texture("Player.png");
        animationFrames = TextureRegion.split(texture,
                texture.getWidth() / 4,
                texture.getHeight() / 4);

        coneLight = new ConeLight(gameWorld.rayHandler, 45, Color.GREEN, 350, 0,0,45,25);
//        coneLight.attachToBody(body,0,0);
//        coneLight.
        coneLight.setColor(new Color(0.2f,0.1f,0,1f));

        pointLight = new PointLight(gameWorld.rayHandler, 180, Color.YELLOW, 250 , 0,0);
        pointLight.setColor(new Color(0.2f,0.1f,0,1f));
        pointLight.attachToBody(body);
        pointLight.setSoftnessLength(100.0f);

        moveAnimation[0] = new Animation<> (1f/8f, animationFrames[2]);
        moveAnimation[1] = new Animation<> (1f/8f, animationFrames[3]);
        moveAnimation[2] = new Animation<> (1f/8f, animationFrames[1]);
        moveAnimation[3] = new Animation<> (1f/8f, animationFrames[0]);
        standAnimation[0] = new Animation<>( 1f/8f, animationFrames[2][0]);
        animation = standAnimation[0];

        maxHp = 100;
        hp = 100;

    }

    public void shot(Vector2 direction)
    {
        if(shootCooldown > 0)
        {
            Bullet bullet = new Bullet(gameWorld, getPosition(), direction);
            gameWorld.addObjectToSimulation(bullet);
            shootCooldown = -1/attackSpeed;
        }
    }

    public void draw(Batch batch)
    {
        TextureRegion frame = animation.getKeyFrame(animationTime,true);
        batch.draw(frame ,
                getPosition().x - frame.getRegionWidth()/2f,
                getPosition().y - frame.getRegionHeight()/8f);
    }

    public void step(float dTime)
    {
        super.step(dTime);


        float angle = body.getLinearVelocity().angleDeg();

        coneLight.setDirection(angle);
        coneLight.setPosition(body.getPosition());
//        coneLight.update();
        int direction = (int) (((angle+45)%360)/90);

        if( body.getLinearVelocity().len() > 0 )
        {
            animation = moveAnimation[direction];
        }
        else
        {
            animation = standAnimation[0];
        }

        shootCooldown += dTime;

    }
}
